package com.ashish.tech.lockerservice.repository;

import com.ashish.tech.lockerservice.models.Locker;

import java.util.List;

public interface ILockerRepository {

    Locker findById(long lockerId);

    void assignOTP(long lockerId,String otp);

    String getAssignedOTP(long lockerId);

    List<Locker> getAllAvailableLockers();
    List<Locker> getAllLockers();

}
