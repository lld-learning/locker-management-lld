package com.ashish.tech.lockerservice.repository;

import com.ashish.tech.lockerservice.models.Locker;
import com.ashish.tech.lockerservice.models.Size;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LockerMemoryRepository implements ILockerRepository{
    private final List<Locker> lockers;
    private final Map<Long, String> otpMap;

    public LockerMemoryRepository(int numOfLockers) {
        this.lockers=new ArrayList<>();
        for (int i = 0; i < numOfLockers; i++) {
            Locker locker=new Locker(i,new Size(1,1));
            this.lockers.add(locker);
        }

        this.otpMap = new HashMap<>();
    }

    @Override
    public Locker findById(long lockerId) {
        return lockers.stream()
                .filter(l-> lockerId==l.getId())
                .findAny()
                .orElse(null);
    }

    @Override
    public void assignOTP(long lockerId,String otp) {
        otpMap.put(lockerId,otp);
    }

    @Override
    public String getAssignedOTP(long lockerId) {
        return otpMap.get(lockerId);
    }

    @Override
    public List<Locker> getAllAvailableLockers() {
        return lockers.stream().filter(l->l.getAssignedToCustomer()==null).collect(Collectors.toList());
    }

    @Override
    public List<Locker> getAllLockers() {
        return lockers;
    }
}
