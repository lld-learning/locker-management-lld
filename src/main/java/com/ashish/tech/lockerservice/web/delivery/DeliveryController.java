package com.ashish.tech.lockerservice.web.delivery;

import com.ashish.tech.lockerservice.models.Locker;
import com.ashish.tech.lockerservice.models.LockerItem;
import com.ashish.tech.lockerservice.service.LockerManagementService;

public class DeliveryController {
    private final LockerManagementService lockerManagementService;

    public DeliveryController(LockerManagementService lockerManagementService) {
        this.lockerManagementService = lockerManagementService;
    }

    public void addItemToLocker(Locker locker, LockerItem lockerItem){
        lockerManagementService.addItemToLocker(locker.getAssignedToCustomer(),locker,lockerItem);
    }
}
