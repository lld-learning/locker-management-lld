package com.ashish.tech.lockerservice.web.admin;

import com.ashish.tech.lockerservice.models.Locker;
import com.ashish.tech.lockerservice.service.LockerManagementService;

import java.util.List;

public class LockerController {
    private final LockerManagementService lockerManagementService;

    public LockerController(LockerManagementService lockerManagementService) {
        this.lockerManagementService = lockerManagementService;
    }

    public List<Locker> getAllLockers(){
        return lockerManagementService.getAllLockers();
    }

    public List<Locker> getAllAvailableLockers(){
        return lockerManagementService.getAllAvailableLockers();
    }

    public void vacateAllLockers(){
        lockerManagementService.vacateAllLockers();
    }
}
