package com.ashish.tech.lockerservice.web.customer;

import com.ashish.tech.lockerservice.exceptions.InvalidOTPException;
import com.ashish.tech.lockerservice.models.Customer;
import com.ashish.tech.lockerservice.models.Locker;
import com.ashish.tech.lockerservice.models.LockerItem;
import com.ashish.tech.lockerservice.service.LockerManagementService;

public class OrderController {
    private final LockerManagementService lockerManagementService;

    public OrderController(LockerManagementService lockerManagementService) {
        this.lockerManagementService = lockerManagementService;
    }

    public Locker allocateLocker(Customer customer, LockerItem lockerItem){
        return lockerManagementService.allocateLocker(customer,lockerItem);
    }

    public LockerItem receiveOrder(Customer customer,Locker locker,String otp) throws InvalidOTPException {
        boolean unlocked=lockerManagementService.unlockLocker(locker.getId(),otp);
        if(unlocked)
            return lockerManagementService.getItemInLocker(locker.getId());
        else
            return null;
    }
}
