package com.ashish.tech.lockerservice.web.customer;

import com.ashish.tech.lockerservice.models.DeliveryPerson;
import com.ashish.tech.lockerservice.models.Locker;
import com.ashish.tech.lockerservice.models.LockerItem;
import com.ashish.tech.lockerservice.service.DeliveryPersonService;
import com.ashish.tech.lockerservice.service.LockerManagementService;

public class ReturnController {
    private final LockerManagementService lockerManagementService;
    private final DeliveryPersonService deliveryPersonService;

    public ReturnController(LockerManagementService lockerManagementService, DeliveryPersonService deliveryPersonService) {
        this.lockerManagementService = lockerManagementService;
        this.deliveryPersonService = deliveryPersonService;
    }

    public void addItemToLocker(Locker locker, LockerItem lockerItem){
        DeliveryPerson deliveryPerson=deliveryPersonService.getDeliveryPerson(locker);
        lockerManagementService.addItemToLocker(deliveryPerson,locker,lockerItem);
    }
}
