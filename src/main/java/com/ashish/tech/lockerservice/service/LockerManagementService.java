package com.ashish.tech.lockerservice.service;

import com.ashish.tech.lockerservice.exceptions.InvalidOTPException;
import com.ashish.tech.lockerservice.exceptions.LockerEmptyException;
import com.ashish.tech.lockerservice.exceptions.LockerFullException;
import com.ashish.tech.lockerservice.models.Customer;
import com.ashish.tech.lockerservice.models.Locker;
import com.ashish.tech.lockerservice.models.LockerItem;
import com.ashish.tech.lockerservice.models.LockerUser;
import com.ashish.tech.lockerservice.repository.ILockerRepository;
import com.ashish.tech.lockerservice.strategies.ILockerAllocationStrategy;
import lombok.NonNull;

import java.util.List;

public class LockerManagementService {
    private final ILockerRepository lockerRepository;
    private final ILockerAllocationStrategy allocationStrategy;
    private final OTPService otpService;

    public LockerManagementService(ILockerRepository lockerRepository,
                                   ILockerAllocationStrategy allocationStrategy,
                                   OTPService otpService) {
        this.lockerRepository = lockerRepository;
        this.allocationStrategy = allocationStrategy;
        this.otpService = otpService;
    }

    public Locker allocateLocker(Customer customer, LockerItem lockerItem){
        Long lockerId=allocationStrategy.getAvailableLocker(customer);
        Locker locker=lockerRepository.findById(lockerId);
        locker.setAssignedToCustomer(customer);
        allocationStrategy.allocateLocker(locker);

        return locker;
    }

    public void addItemToLocker(LockerUser notifyLockerUser, Locker locker, LockerItem lockerItem){
        Locker storedLocker=lockerRepository.findById(locker.getId());
        if(storedLocker.isEmpty()){
            storedLocker.addItemToLocker(lockerItem);
            String otp= otpService.generateOTP();
            lockerRepository.assignOTP(storedLocker.getId(), otp);
            otpService.sendOTP(notifyLockerUser,otp);
        } else {
            throw new LockerFullException();
        }
    }

    public List<Locker> getAllAvailableLockers(){
        return lockerRepository.getAllAvailableLockers();
    }

    public List<Locker> getAllLockers(){
        return lockerRepository.getAllLockers();
    }

    public void vacateAllLockers(){
        //todo
    }

    public boolean unlockLocker(long lockerId,String otp) throws InvalidOTPException{
        String storedOTP=lockerRepository.getAssignedOTP(lockerId);
        if(storedOTP==null || !storedOTP.equals(otp))
            throw new InvalidOTPException();
        return true;
    }

    public LockerItem getItemInLocker(long lockerId){
        LockerItem lockerItem=lockerRepository.findById(lockerId).getLockerItem();
        if(lockerItem==null)
            throw new LockerEmptyException();
        Locker locker=lockerRepository.findById(lockerId);
        locker.vacateLocker();
        return lockerItem;

    }
}
