package com.ashish.tech.lockerservice.service;

import com.ashish.tech.lockerservice.models.LockerUser;
import com.ashish.tech.lockerservice.models.MsgType;
import com.ashish.tech.lockerservice.models.NotificationParams;
import com.ashish.tech.lockerservice.strategies.IOTPGenerationStrategy;

import java.util.HashMap;
import java.util.Map;

public class OTPService {
    private final NotificationService notificationService;
    private final IOTPGenerationStrategy otpGenerationStrategy;
    private final int numOfChars;

    public OTPService(NotificationService notificationService, IOTPGenerationStrategy otpGenerationStrategy, int numOfChars) {
        this.notificationService = notificationService;
        this.otpGenerationStrategy = otpGenerationStrategy;
        this.numOfChars = numOfChars;
    }

    public String generateOTP(){
        return otpGenerationStrategy.generateOTP(numOfChars);
    }
    public void sendOTP(LockerUser lockerUser, String otp){
        //send the otp using Notification Service
        Map<String,String> params=new HashMap<>();
        params.put(NotificationParams.OTP.value,otp);
        notificationService.sendNotification(lockerUser, MsgType.SMS_OTP,params);
    }
}
