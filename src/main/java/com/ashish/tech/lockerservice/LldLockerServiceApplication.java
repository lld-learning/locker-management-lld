package com.ashish.tech.lockerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LldLockerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LldLockerServiceApplication.class, args);
	}

}
