package com.ashish.tech.lockerservice.models;

import lombok.Getter;

public class LockerUser {

    @Getter
    private Contact contact;

    public LockerUser(Contact contact) {
        this.contact = contact;
    }
}
