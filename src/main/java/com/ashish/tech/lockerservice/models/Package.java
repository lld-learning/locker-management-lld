package com.ashish.tech.lockerservice.models;

import lombok.Getter;

public class Package implements LockerItem{
    private long id;
    private Size size;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public Size getSize() {
        return size;
    }

    public Package(long id, Size size) {
        this.id = id;
        this.size = size;
    }
}
