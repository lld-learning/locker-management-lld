package com.ashish.tech.lockerservice.models;

public interface LockerItem {

    long getId();
    Size getSize();
}
