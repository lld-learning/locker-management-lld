package com.ashish.tech.lockerservice.models;

public class DeliveryPerson extends LockerUser {
    public DeliveryPerson(Contact contact) {
        super(contact);
    }
}
