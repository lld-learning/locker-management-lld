package com.ashish.tech.lockerservice.models;

public enum NotificationParams {
    OTP("otp");

    public String value;
    NotificationParams(String value) {
        this.value=value;
    }
}
