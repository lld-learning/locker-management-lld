package com.ashish.tech.lockerservice.models;

import lombok.NonNull;

public class Size {
    int height;
    int width;

    public Size(@NonNull int height,@NonNull int width) {
        this.height = height;
        this.width = width;
    }
}
