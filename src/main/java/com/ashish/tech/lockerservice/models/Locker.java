package com.ashish.tech.lockerservice.models;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

public class Locker {
    @Getter
    private long id;
    private Size capacity;
    @Getter
    @Setter
    private Customer assignedToCustomer;
    @Getter
    private LockerItem lockerItem;
    private boolean isLocked;

    public Locker(long id, Size capacity) {
        this.id = id;
        this.capacity = capacity;
    }

    public void addItemToLocker(@NonNull LockerItem lockerItem){
        //check for capacity
        this.lockerItem=lockerItem;
        isLocked=true;

    }

    public boolean isEmpty(){
        return lockerItem==null;
    }

    public void vacateLocker(){
        assignedToCustomer=null;
        lockerItem=null;
        isLocked=false;
    }

    public boolean canAccommodate(@NonNull final Size sizeToAccommodate) {
        return this.capacity.width >= sizeToAccommodate.width && this.capacity.height >= sizeToAccommodate.height;
    }
}
