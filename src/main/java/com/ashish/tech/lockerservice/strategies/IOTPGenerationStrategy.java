package com.ashish.tech.lockerservice.strategies;

public interface IOTPGenerationStrategy {

    String generateOTP(int numOfDigits);
}
