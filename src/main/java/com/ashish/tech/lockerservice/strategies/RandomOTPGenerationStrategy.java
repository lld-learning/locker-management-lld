package com.ashish.tech.lockerservice.strategies;

public class RandomOTPGenerationStrategy implements IOTPGenerationStrategy{
    @Override
    public String generateOTP(int numOfDigits) {
        return String.valueOf((int)(Math.random()*10000));
    }
}
