package com.ashish.tech.lockerservice.strategies;

import com.ashish.tech.lockerservice.exceptions.InvalidLockerException;
import com.ashish.tech.lockerservice.exceptions.NoLockerFoundException;
import com.ashish.tech.lockerservice.models.Customer;
import com.ashish.tech.lockerservice.models.Locker;

import java.util.List;
import java.util.stream.Collectors;

public class RandomAllocationStrategy implements ILockerAllocationStrategy{
    List<Long> lockers=null;

    public RandomAllocationStrategy(List<Locker> lockers) {
        this.lockers = lockers.stream().map(Locker::getId).collect(Collectors.toList());
    }

    @Override
    public Long getAvailableLocker(Customer customer) throws NoLockerFoundException {
        if(lockers==null || lockers.isEmpty())
            throw new NoLockerFoundException();
        return (long)(Math.random()*lockers.size());
    }

    @Override
    public void allocateLocker(Locker locker) throws InvalidLockerException {
        lockers.remove(locker.getId());
    }

    @Override
    public void deallocateLocker(Locker locker) throws InvalidLockerException {

    }
}
