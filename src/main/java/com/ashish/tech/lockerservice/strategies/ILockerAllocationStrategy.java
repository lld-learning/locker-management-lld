package com.ashish.tech.lockerservice.strategies;

import com.ashish.tech.lockerservice.exceptions.InvalidLockerException;
import com.ashish.tech.lockerservice.exceptions.NoLockerFoundException;
import com.ashish.tech.lockerservice.models.Customer;
import com.ashish.tech.lockerservice.models.Locker;

public interface ILockerAllocationStrategy {

    Long getAvailableLocker(Customer customer) throws NoLockerFoundException;

    void allocateLocker(Locker locker) throws InvalidLockerException;

    void deallocateLocker(Locker locker) throws InvalidLockerException;
}
