package com.ashish.tech.lockerservice.strategies;

import com.ashish.tech.lockerservice.models.Locker;
import com.ashish.tech.lockerservice.models.LockerItem;
import lombok.NonNull;

import java.util.List;
import java.util.concurrent.locks.Lock;

public interface ILockerFilterStrategy {
    @NonNull
    List<Locker> filterSlots(@NonNull List<Locker> slots, @NonNull LockerItem lockerItem);
}
