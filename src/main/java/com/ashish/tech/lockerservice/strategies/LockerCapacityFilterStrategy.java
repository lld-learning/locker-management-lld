package com.ashish.tech.lockerservice.strategies;

import com.ashish.tech.lockerservice.models.Locker;
import com.ashish.tech.lockerservice.models.LockerItem;
import lombok.NonNull;

import java.util.List;

public class LockerCapacityFilterStrategy implements ILockerFilterStrategy{
    @Override
    public @NonNull List<Locker> filterSlots(@NonNull List<Locker> lockers, @NonNull LockerItem lockerItem) {
        return lockers;
    }
}
