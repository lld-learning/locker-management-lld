package com.ashish.tech.lockerservice;

import com.ashish.tech.lockerservice.exceptions.NoLockerFoundException;
import com.ashish.tech.lockerservice.models.*;
import com.ashish.tech.lockerservice.models.Package;
import com.ashish.tech.lockerservice.repository.LockerMemoryRepository;
import com.ashish.tech.lockerservice.service.DeliveryPersonService;
import com.ashish.tech.lockerservice.service.LockerManagementService;
import com.ashish.tech.lockerservice.service.NotificationService;
import com.ashish.tech.lockerservice.service.OTPService;
import com.ashish.tech.lockerservice.strategies.RandomAllocationStrategy;
import com.ashish.tech.lockerservice.web.admin.LockerController;
import com.ashish.tech.lockerservice.web.customer.OrderController;
import com.ashish.tech.lockerservice.web.customer.ReturnController;
import com.ashish.tech.lockerservice.web.delivery.DeliveryController;
import com.ashish.tech.lockerservice.web.delivery.PickupController;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LockerTest {
    OrderController orderController;
    ReturnController returnController;
    DeliveryController deliveryController;
    PickupController pickupController;
    LockerController lockerController;
    @Mock
    NotificationService notificationService;
    @Mock
    OTPService otpService;
    @Before
    public void setup(){
        LockerMemoryRepository lockerMemoryRepository=new LockerMemoryRepository(1);
        LockerManagementService lockerManagementService=new LockerManagementService(lockerMemoryRepository,
                new RandomAllocationStrategy(lockerMemoryRepository.getAllLockers()),
                otpService);
        orderController=new OrderController(lockerManagementService);
        returnController=new ReturnController(lockerManagementService,new DeliveryPersonService());
        deliveryController=new DeliveryController(lockerManagementService);
        lockerController=new LockerController(lockerManagementService);
        pickupController=new PickupController();
    }

    @Test
    public void addingItemToLocker(){
        //Arrange
        Customer customer=new Customer(new Contact());
        LockerItem lockerItem=new Package(0,new Size(1,1));

        //Act
        Locker locker=orderController.allocateLocker(customer,lockerItem);

        //Assert
        Assertions.assertFalse(lockerController.getAllAvailableLockers().contains(locker));
    }

    @Test(expected = NoLockerFoundException.class)
    public void lockersFullTest(){
        Customer customer=new Customer(new Contact());
        LockerItem lockerItem=new Package(1,new Size(1,1));

        Customer customer1=new Customer(new Contact());
        LockerItem lockerItem1=new Package(1,new Size(1,1));

        //Act
        Locker locker=orderController.allocateLocker(customer,lockerItem);
        Locker locker1=orderController.allocateLocker(customer1,lockerItem1);
    }

    @Test
    public void deallocationTest(){
        //Arrange
        Customer customer=new Customer(new Contact());
        LockerItem lockerItem=new Package(0,new Size(1,1));
//        when(otpService.generateOTP()).thenReturn("1234");
//        doNothing().when(otpService).sendOTP(customer,"1234");

        //Act
        Locker locker=orderController.allocateLocker(customer,lockerItem);
        deliveryController.addItemToLocker(locker,lockerItem);

        //Assert
        ArgumentCaptor<String> otpCaptor = ArgumentCaptor.forClass(String.class);
        verify(otpService).sendOTP(eq(customer),otpCaptor.capture());
        final String otp = otpCaptor.getValue();
        assertNotNull(otp);

        Assertions.assertEquals(orderController.receiveOrder(customer, locker, otp).getId(), lockerItem.getId());

        Assertions.assertTrue(lockerController.getAllAvailableLockers().contains(locker));
    }
}
